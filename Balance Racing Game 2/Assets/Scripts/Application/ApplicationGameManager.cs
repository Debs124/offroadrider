﻿// <copyright file="Application.cs" company="Zabingo Softwares">
// Copyright (C) 2017 Zabingo Softwares.All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

using System.Collections;
using System.Collections.Generic;
using Becker.MVC;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.UI;

namespace RideHard {
	
	public class ApplicationGameManager : BaseApplication<GameModel,GameView,GameController> {

		string path;
		string GetDetailsInString;
		string[] SplitDetails;
		GameObject HoldinCarInstance;
		void Awake(){
			path = Application.persistentDataPath + "1334rty.txt";
			if (File.Exists (path)) {
			
				GetUnlockedCar (path);
			} else
				SetUnlockPathOrWrite (path, "1");

			model.AllCoins	= view._getScore_storeScore.WroteFromFile_Score ();
			model.TotalCoins.text = model.AllCoins.ToString();
		}

		void Start(){
			
			if (model._audModels.PlayAudioAll)
				model._audModels.BackgroundMusic.Play ();
		}

		void GetUnlockedCar( string pathToFetch){

			StreamReader reader = new StreamReader (path, true);
			GetDetailsInString = reader.ReadToEnd ();
			SplitDetails = GetDetailsInString.Split (' ');
			//Debug.Log (CollectedCoins);
			reader.Close ();
			unlockCars ();
		}

		void SetUnlockPathOrWrite(string UnlockedCarPath, string ToWrite)
		{
			StreamWriter writer = new StreamWriter (UnlockedCarPath,true);
			writer.Write (ToWrite + " ");
			writer.Close ();

		}

		void unlockCars ()
		{
			Debug.Log (SplitDetails.Length.ToString ());
			foreach (string getDetails in SplitDetails) {
				GameObject CarPurachasePanel = null;
				Debug.Log ("the Tag: " + getDetails);
				if (getDetails.Equals ("2")) {
					HoldinCarInstance = model.CarToBuy_1;
					CarPurachasePanel = model.CarToBuyPurchasePanel_1;
				}
				if(CarPurachasePanel != null)
				CarPurachasePanel.SetActive (false);
				//HoldinCarInstance = GameObject.FindGameObjectWithTag (getDetails);
				if (HoldinCarInstance != null) {
					HoldinCarInstance.GetComponent<Button> ().enabled = true;
					HoldinCarInstance.GetComponent<Image> ().color = new Color (255f, 255f, 255f, 1.0f);
				}


			}
		}

		public void UnlockedDirectly(string Tag)
		{
			GameObject CarPurachasePanel = null;
			int amount =0;
			if (Tag.Equals ("2")) {
				amount = 5000;
			}
			if(amount <= model.AllCoins){

				if (Tag.Equals ("2")) {
					HoldinCarInstance = model.CarToBuy_1;
					CarPurachasePanel = model.CarToBuyPurchasePanel_1;
				}
				if(CarPurachasePanel != null)
					CarPurachasePanel.SetActive (false);

				if (HoldinCarInstance != null) {
					HoldinCarInstance.GetComponent<Button> ().enabled = true;
					HoldinCarInstance.GetComponent<Image> ().color = new Color (255f, 255f, 255f, 1.0f);
					SetUnlockPathOrWrite (path, Tag);
				}

				view._getScore_storeScore.WriteIntoFile_Score ((model.AllCoins - amount).ToString ());

				view._getScore_storeScore.CollectedCoins -= amount;
				model.TotalCoins.text = view._getScore_storeScore.CollectedCoins.ToString ();
				return;

		}
		}

	}
}
