﻿using UnityEngine.SceneManagement;
using UnityEngine;
using Becker.MVC;
using System.Collections;

namespace RideHard{
	public class PlayGame : Controller<ApplicationGameManager>  {

		public GameObject Loading;
		public Animator QuitConformation;
		public Animator creditPanel;

		public void Play(){
			Loading.SetActive (true);
			SceneManager.LoadSceneAsync (2);
		}

		public void QuitConfirm(){
			QuitConformation.SetBool("HideDialouge",false);
			QuitConformation.SetBool("ShowDialouge",true);
		}
		public void QuitCanceled(){
			QuitConformation.SetBool("ShowDialouge",false);
			QuitConformation.SetBool("HideDialouge",true);

		}

		public void Credit(){
			creditPanel.SetBool ("Credit", true);

		}
		public void GoBack(){
			
			creditPanel.SetBool ("Credit", false);

		}

		public void IfQuitSelected(){
		
			Application.Quit ();
		}

}
}