﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Becker.MVC;

namespace RideHard{
	public class CoinCollectModel : Model<ApplicationGameManager> {
		public int Count;
		public Text CoinCounter;

		[HideInInspector]
		public int coinCounter = 0;

		public Image Nitro;

}
}