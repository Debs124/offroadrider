﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class AudioModels : Model<ApplicationGameManager> {

		public AudioSource CarAudio,coinAudio,blastAudio, BackgroundMusic;
		public bool PlayAudioAll,PlayOnlyFX,PlayOnlyMusic;
}
}