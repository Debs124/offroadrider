﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class BackgroundScolling : Model<ApplicationGameManager> {

	public Transform cameraTransform;
	public Transform[] layers;
	public float viewZone =6f;
	public int leftIndex;
	public int rightIndex;
	public float backgroungSize;
	[SerializeField]
	public bool isScrolling, isParallaxing;
	public float lastCameraX;
	[SerializeField]
	public float parallaxSpeed=0.4f;

		public float counter;
	public float ParallaxSpeed {
		get{ return parallaxSpeed; }
	}
	public bool IsScrolling {
		get{ return isScrolling;}
	}

	public bool IsParallaxing {
		get{return isParallaxing ;}
	}

		public void LandDecided()
		{
			if (app.model.LandSelection.Equals ("Grass"))
				backgroungSize = 65f;
			else 
				backgroungSize = 64.8f;
		}
}
}