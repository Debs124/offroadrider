﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;
using TMPro;

namespace RideHard{
	public class GameOver : Controller<ApplicationGameManager> {
	
		public GameObject GameOverBackground, FuelOver;
		public TextMeshProUGUI Distance, TotalCoins;

		public void OverGame(string GetStats){

		
			switch (GetStats) {

			case "SawBlade":
				app.model.CamJerk.SetBool ("JerkNow", true);
				Handheld.Vibrate ();
				if (app.model.carSelect.SelectedCar == "Car1") {
                            
					app.model.carModel.backAndFrontWheel_car1 [0].enabled = false;
					app.model.carModel.backAndFrontWheel_car1 [1].enabled = false;
					//app.model.carModel.Car1.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
				}
				if (app.model.carSelect.SelectedCar == "Car2") {
                            
					app.model.carModel.backAndFrontWheel_car2 [0].enabled = false;
					app.model.carModel.backAndFrontWheel_car2 [1].enabled = false;
					//app.model.carModel.Car2.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;
				}

				Instantiate (app.model.Blast, app.model.carModel.CarRigid.transform.position, app.model.carModel.CarRigid.transform.rotation);
				Destroy (app.model.carModel.CarRigid);
				app.controller.InPlayMode = false;

                    //Debug.Log ("Game Over");
                    break;

			case "FuelOver":
                FuelOver.SetActive (true);	
				app.controller.InPlayMode = false;
				break;

			case "Car Turned":
                    app.controller.InPlayMode = false;
                    app.model.CurTurnedGameOverModel.SetActive(true);
				break;

			case "RockBlast":
				app.controller.InPlayMode = false;
				break;
			}
			StartCoroutine ("GameOverStats");




		}

		IEnumerator GameOverStats(){
		
			yield return new WaitForSeconds (2f);
			GameOverBackground.SetActive (true);
			Distance.text 	= app.view.DstCalc.distance.ToString () + "M";
			TotalCoins.text = app.model.coinCollect.Count.ToString () + "$";
			app.view._getScore_storeScore.WriteIntoFile_Score ((app.model.AllCoins + app.model.coinCollect.Count).ToString ());
			StopCoroutine ("GameOverStats");

		}
}
}