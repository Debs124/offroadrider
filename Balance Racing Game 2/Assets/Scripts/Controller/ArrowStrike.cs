﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class ArrowStrike : Controller<ApplicationGameManager> {

		//public Animator ArrowStrik;

		void OnTriggerEnter2D(Collider2D col){
			if (col.gameObject.tag == "Car")
				app.controller._gmOver.OverGame ("RockBlast");
		}
}
}