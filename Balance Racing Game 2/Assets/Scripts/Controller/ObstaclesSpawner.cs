﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class ObstaclesSpawner : Controller<ApplicationGameManager> {

		public GameObject[] obstacles = new GameObject[4];
		public GameObject[] Bridge;
		public Transform spawnPoint1,SpawnPoint2,SpawnPoint3;
		[HideInInspector]
		public GameObject HoldForBlade,HoldForRock;
		[HideInInspector]
		public bool notSpawned = true;
		public Transform CoinsAndObstacles,MapMain3;
		int spwanSelector, ObstaclesSelector;

		public void SpawnObstacles(){
		
			spwanSelector = Random.Range (0, 4);
			ObstaclesSelector = Random.Range (0, 4);

			if(app.controller.AllowObstacles)
			StartCoroutine ("SpwanObstacles");
		}

		IEnumerator SpwanObstacles(){

			while (true) {
				if (app.controller.InPlayMode) {
					if (app.model.carModel.rb.velocity.x > 3f) {
		
						switch (spwanSelector) {

						case 0:
							HoldForBlade = Instantiate (obstacles [0], spawnPoint1.position, spawnPoint1.rotation);
							HoldForBlade.transform.position = new Vector3 (spawnPoint1.position.x, 2.0f, -0.59f);
							Debug.Log ("Blade Spawned");
							break;

						case 1:
							HoldForRock = Instantiate (obstacles [1], SpawnPoint2.position, SpawnPoint2.rotation);
							HoldForRock.transform.position = new Vector3 (SpawnPoint2.position.x, 3.60f, -0.59f);
							Debug.Log ("Rock Spawned");
							break;
						case 2:
							HoldForRock = Instantiate (obstacles [2], spawnPoint1.position, spawnPoint1.rotation);
							HoldForRock.transform.position = new Vector3 (spawnPoint1.position.x, 3.60f, -0.59f);
							Debug.Log ("Slider Spawned");
							break;

						case 3:
							HoldForBlade = Instantiate (obstacles [3], spawnPoint1.position, spawnPoint1.rotation);
							HoldForBlade.transform.position = new Vector3 (spawnPoint1.position.x, 7f, -0.59f);
							Debug.Log ("Propeler Spawned");
							break;
							
						}
						spwanSelector = Random.Range (0, 4);
						ObstaclesSelector = Random.Range (0, 4);
					}
				}
				yield return new WaitForSeconds (15f);
			
			}
		}
}
}