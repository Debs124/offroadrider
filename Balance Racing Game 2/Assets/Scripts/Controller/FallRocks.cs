﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class FallRocks : Controller<ApplicationGameManager> {

		//public GameObject Blast,fatol1,fatol2;
		Rigidbody2D RockRigid;
		public Transform BlastPos;

		void Start(){
		
			RockRigid = GetComponent<Rigidbody2D> ();

		}

		void OnCollisionEnter2D(Collision2D col){
			
			//HoldInstance.transform.parent = transform.parent;

			if (col.gameObject.tag == "Car") {
				//app.model.CamJerk.SetBool ("JerkNow", true);
				app.controller._gmOver.OverGame ("SawBlade");
			}
			else {
				app.model.CamJerk.SetBool ("JerkNow", true);
				app.model._audModels.blastAudio.Play ();
				BlastPos.position = new Vector3 (transform.position.x, transform.position.y, -9f);
				GameObject HoldInstance = Instantiate (app.model.Blast, BlastPos.position, BlastPos.rotation);
				Destroy (this.gameObject);
				//fatol1.SetActive(true);
				//fatol2.SetActive(true);
			}

		}
}
}
