﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Becker.MVC;
using System;
using UnityEngine.UI;

namespace RideHard{
	public class Upgrade : Controller<ApplicationGameManager> {
		[HideInInspector]
		public string FileToDestine, TotalDetails, CarEngineLevel, NitroLevel;

		string[] CarInfos;
		[HideInInspector]
		public int engine, nitro, desiredCoin, desiredCoin_Nitro;

		int CounterOfLevel = 1, CounterOfLevel_nitro = 1;

		bool notUpgradebale = false, DoUpgradeCar = false, notUpgradeable_Nitro = false, DoUpgradeNitro = false;

		//string[] Levels = new string[5] ;
		public Animator UpgradeAnim,UpgradeAnim_Car2;

		public Button Engine,Nitro;
		public Text EngineText, NitroText;
		public GameObject Message, UpgradePanel;

		void Start(){
		}

		public void UpgradeCarStatus(){
			FileToDestine = Application.persistentDataPath + "/" + app.model.carSelect.SelectedCar + ".txt";
			CheckFirst (FileToDestine, app.model.carSelect.SelectedCar);
		}

		/// <summary>
		/// checking the car status, of selected cars. CheckFirst_Extended is actually deciding the values, this function is the reading the file.
		/// </summary>
		/// <returns>The first.</returns>
		/// <param name="FilePath">File path.</param>
		/// <param name="CarName">Car name.</param>
		void CheckFirst(string FilePath, string CarName){

			if (File.Exists (FilePath)) {
			
				StreamReader reader = new StreamReader (FilePath, true);
				TotalDetails = reader.ReadToEnd ();
				CarInfos = TotalDetails.Split (' ');
				engine = Convert.ToInt32 (CarInfos [0]);
				nitro = Convert.ToInt32 (CarInfos [1]);
				app.model.carModel.CarEnigineCapacity = engine;
				app.model.carModel.NitroSpeed = nitro;
				reader.Close ();

				//Calling those function to check engine status and nitro status of selected cars
				CheckFirst_Extended(engine);
				CheckFirstExtended_Nitro (nitro);
				//END

				EngineText.text = CarEngineLevel;
				NitroText.text = nitro.ToString () + " SG";
				CheckIfUpgradeableOrNot ();
				CheckIfUpgradeableOrNot_Nitro ();
				UpgradeAnim.SetBool ("Upgrade", true);
				} 

			else {
				app.controller._chooseCar.SetDataInFileOfCarSpecs ("100", "30", FileToDestine);
			}

		}
		/// <summary>
		/// Checks the first extended engine.
		/// </summary>
		/// <returns>The first extended.</returns>
		/// <param name="engine">Engine.</param>
		void CheckFirst_Extended(int engine)
		{
			switch (engine) {

			case 200:
				CounterOfLevel = 1;
				CarEngineLevel = "Level1";
				desiredCoin = 1000;
				notUpgradebale = false;
				DoUpgradeCar = true;
				break;

			case 400:
				CounterOfLevel = 2;
				CarEngineLevel = "Level2";
				desiredCoin = 3000;
				notUpgradebale = false;
				DoUpgradeCar = true;
				break;
			case 600:
				CounterOfLevel = 3;
				CarEngineLevel = "Level3";
				desiredCoin = 5000;
				notUpgradebale = false;
				DoUpgradeCar = true;
				break;
			case 800:
				CounterOfLevel = 4;
				CarEngineLevel = "Level4";
				desiredCoin = 7000;
				notUpgradebale = false;
				DoUpgradeCar = true;
				break;
			case 1000:
				CounterOfLevel = 5;
				CarEngineLevel = "Level5";
				desiredCoin = 9000;
				notUpgradebale = true;
				DoUpgradeCar = false;
				break;

			}
			if(desiredCoin >= 9000)
				app.model.DesiredCoinsEngine.text = "MAX LEVEL";
			else
			app.model.DesiredCoinsEngine.text = desiredCoin.ToString ();
		}

		/// <summary>
		/// Checks the first extended nitro.
		/// </summary>
		/// <returns>The first extended nitro.</returns>
		/// <param name="nitro">Nitro.</param>
		void CheckFirstExtended_Nitro(int nitro){
		
			switch (nitro) {

			case 30:
				CounterOfLevel_nitro = 1;
				desiredCoin_Nitro = 500;
				DoUpgradeNitro = true;
				notUpgradeable_Nitro = false;
				break;
			case 40:
				CounterOfLevel_nitro = 2;
				desiredCoin_Nitro = 1500;
				DoUpgradeNitro = true;
				notUpgradeable_Nitro = false;
				break;
			case 50:
				CounterOfLevel_nitro = 3;
				desiredCoin_Nitro = 3000;
				DoUpgradeNitro = true;
				notUpgradeable_Nitro = false;
				break;
			case 60:
				CounterOfLevel_nitro = 4;
				desiredCoin_Nitro = 5000;
				DoUpgradeNitro = false;
				notUpgradeable_Nitro = true;
				break;
					
			}
			if(desiredCoin_Nitro >= 5000)
				app.model.DesiredCoinsNitro.text = "MAX LEVEL";
			else
			app.model.DesiredCoinsNitro.text = desiredCoin_Nitro.ToString ();
		}

		/// <summary>
		/// Checks if upgradeable or not.
		/// </summary>
		/// <returns>The if upgradeable or not.</returns>
		void CheckIfUpgradeableOrNot (){
		
			if (!notUpgradebale) {
			
				if (app.view._getScore_storeScore.CollectedCoins >= desiredCoin) {
				
					DoUpgradeCar = true;
				
				} else
					DoUpgradeCar = false;
			}
		}

		void CheckIfUpgradeableOrNot_Nitro (){
		
			if (!notUpgradeable_Nitro) {
			
				if (app.view._getScore_storeScore.CollectedCoins >= desiredCoin_Nitro) {
				
					DoUpgradeNitro = true;
				} else
					DoUpgradeNitro = false;
			}
		}

		/// <summary>
		/// This Function is upgradeing engine only of selected car
		/// </summary>
		/// <returns>The it.</returns>
		public void UpgradeIt()
		{
			
			if (DoUpgradeCar) {
				CounterOfLevel ++;
				app.model.carModel.CarEnigineCapacity += 200f;
				Debug.Log ("Counter Of Level: " + CounterOfLevel);
				EngineText.text = "Level" + CounterOfLevel.ToString ();
				app.view._getScore_storeScore.CollectedCoins -= desiredCoin;
				SetDataInFile ();
				CheckFirst_Extended ((int)app.model.carModel.CarEnigineCapacity);
				CheckIfUpgradeableOrNot ();
			} 
			else
			{
				Message.SetActive(false);

				if(notUpgradebale)
					Message.GetComponent<Text>().text = "Maximum engine upgrade reached!";

				else
					Message.GetComponent<Text>().text = "Sorry! Not enough money";
				Message.SetActive(true);

			} 
				
		}

		public void UpgradeIt_Nitro()
		{
			if (DoUpgradeNitro) {
			
				app.model.carModel.NitroSpeed += 10f;
				NitroLevel = ((int)app.model.carModel.NitroSpeed).ToString () + "SG";
				NitroText.text = NitroLevel;
				app.view._getScore_storeScore.CollectedCoins -= desiredCoin;
				SetDataInFile ();
				CheckFirstExtended_Nitro ((int)app.model.carModel.NitroSpeed);
				CheckIfUpgradeableOrNot_Nitro ();
			} 
			else {
				Message.SetActive(false);
				if (notUpgradeable_Nitro)
					Message.GetComponent<Text> ().text = "Maximum nitro upgrade reached!";
				else
					Message.GetComponent<Text>().text = "Sorry! Not enough money";
				Message.SetActive(true);
			}
			}

		void SetDataInFile()
		{
			app.controller._chooseCar.ResetFileData (FileToDestine);
			app.controller._chooseCar.SetDataInFileOfCarSpecs (app.model.carModel.CarEnigineCapacity.ToString (), app.model.carModel.NitroSpeed.ToString (),FileToDestine);
			app.model.TotalCoins.text = app.view._getScore_storeScore.CollectedCoins.ToString ();
			app.view._getScore_storeScore.WriteIntoFile_Score (app.view._getScore_storeScore.CollectedCoins.ToString());
		}

}
}