// <copyright file="ScrollingBackground.cs" company="Zabingo Softwares">
// Copyright (C) 2017 Zabingo Softwares.All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>


using UnityEngine;
using System.Collections;
using Becker.MVC;

namespace RideHard{
	
	public class ScrollingBackground2 : Controller<ApplicationGameManager> {

		private Transform cameraTransform;
		private Transform[] layers;
		private float viewZone =6f;
		private int leftIndex;
		private int rightIndex;
		private float backgroungSize = 65f;
		[SerializeField]
		private bool isScrolling, isParallaxing;
		private float lastCameraX;
		[SerializeField]
		private float parallaxSpeed=0.4f;


		public float ParallaxSpeed {
			get{ return parallaxSpeed; }
		}
		public bool IsScrolling {
			get{ return isScrolling;}
		}

		public bool IsParallaxing {
			get{return isParallaxing ;}
		}

	// Use this for initialization
	void Start () {
	
			cameraTransform = Camera.main.transform;
			lastCameraX = cameraTransform.position.x;
			layers = new Transform[transform.childCount];
			for (int i = 0; i < transform.childCount; i++) {
				layers [i] = transform.GetChild (i);
			}
			leftIndex = 0;
			rightIndex = layers.Length - 1;
			//isScrolling = true;
			//isParallaxing = true;
			//backgroungSize = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
	}
	
	// Update is called once Physics timestamp
		void  FixedUpdate () {

			if (isParallaxing) {
				float deltaX = cameraTransform.position.x - lastCameraX;
				transform.position += Vector3.right * (deltaX * parallaxSpeed);
			}

			lastCameraX = cameraTransform.position.x;

			if (isScrolling) {
				if (cameraTransform.position.x < (layers [leftIndex].transform.position.x + viewZone)) {
			
					ScrollLeft ();
				}
				if (cameraTransform.position.x > (layers [rightIndex].transform.position.x - viewZone)) {

					ScrollRight ();
				}
			}
		}

		/// <summary>
		/// Scrolls the background to left.
		/// </summary>
		private void ScrollLeft()
		{
			int lastRight = rightIndex;
			layers [rightIndex].position = Vector2.right * (layers [leftIndex].position.x - backgroungSize);
			//layers [rightIndex].position.y = 14.6f;
			leftIndex = rightIndex;
			rightIndex--;
			if (rightIndex < 0) {
				rightIndex = layers.Length - 1;
			}
		}

		/// <summary>
		/// Scrolls the backgroung to right.
		/// </summary>
		private void ScrollRight()
		{
			int lastLeft = leftIndex;
			layers [leftIndex].position =Vector2.right * (layers [rightIndex].position.x + backgroungSize);
			//layers [rightIndex].position.y = 14.6f;
			rightIndex = leftIndex;
			leftIndex++;
			if (leftIndex == layers.Length) {
				leftIndex = 0;
			}
		}
    }
}

