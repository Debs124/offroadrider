using UnityEngine;
using UnityEngine.SceneManagement;
using Becker.MVC;

namespace RideHard{
	public class Goal : View<ApplicationGameManager> {

	void OnTriggerEnter2D (Collider2D colInfo)
	{
		if (colInfo.CompareTag("Player"))
		{
			Debug.Log("GAME WON! :D");
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}

}
}
