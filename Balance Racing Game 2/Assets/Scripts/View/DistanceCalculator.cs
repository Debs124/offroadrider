﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Becker.MVC;

namespace RideHard{
	public class DistanceCalculator : View<ApplicationGameManager> {


		public float PrevCount, newCount;
		public int distance;
		public Text distanceText;

		public void InitCarForDistanceMeasurement(){
			

		}

		public void DistCalculate(){

			if (app.model.carModel.rb.velocity.x > 0) {
				if (app.model.carModel.CarRigid.transform.position.x - PrevCount >= 3f) {
				
					PrevCount = app.model.carModel.CarRigid.transform.position.x;
					distance++;

					distanceText.text = "Distance: "+distance.ToString()+"M";
				//	Debug.Log (distance);
				}
					

			}
		}
}
}