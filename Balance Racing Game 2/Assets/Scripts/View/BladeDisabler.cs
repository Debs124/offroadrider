﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Becker.MVC;

namespace RideHard{
	public class BladeDisabler : Controller<ApplicationGameManager> {

		void OnBecameInvisible(){

			Destroy (app.controller.ObsSpwn.HoldForRock);
		}
}
}