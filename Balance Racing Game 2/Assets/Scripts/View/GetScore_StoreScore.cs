﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Becker.MVC;
using System;

namespace RideHard{
	public class GetScore_StoreScore : View<ApplicationGameManager>{
	
		string path;
		public int CollectedCoins;

		void Awake(){
		path = Application.persistentDataPath + "/Score.txt";
		//File.Create (path).Close();
			WriteFirst ();

			Debug.Log (Application.persistentDataPath);
		}

		void WriteFirst (){
			if (!File.Exists (path)) {
				StreamWriter writer = new StreamWriter (path, true);
				writer.WriteLine ("0");
				writer.Close ();
				return;
			} else
				return;

		}

	
		public void WriteIntoFile_Score(string NewCoins){
			File.Create (path).Close();
			NowWrite_File (NewCoins);

			}

		void NowWrite_File(string NewCoins){
		
			StreamWriter writer = new StreamWriter(path, true);
			writer.WriteLine(NewCoins);
			writer.Close();
		}
	


		public int WroteFromFile_Score(){

			StreamReader reader = new StreamReader (path, true);
			CollectedCoins = Convert.ToInt32(reader.ReadLine ());
			Debug.Log (CollectedCoins);
			reader.Close ();
			return CollectedCoins;

		}
}
}